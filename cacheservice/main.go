package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/cyberdisk/jaeger-rd/util"
	opentracing "github.com/opentracing/opentracing-go"
)

type cacheEntry struct {
	data       []byte
	expiration time.Time
}

var (
	cache  = make(map[string]*cacheEntry)
	mutex  = sync.RWMutex{}
	tickCh = time.Tick(5 * time.Second)
)

var (
	tracer opentracing.Tracer
	closer io.Closer
)

var maxAgeRexexp = regexp.MustCompile(`maxage=(\d+)`)

func main() {
	tracer, closer = util.InitJaeger("cache-service")
	defer closer.Close()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodGet {
			getFromCache(w, r)
		} else if r.Method == http.MethodPost {
			saveToCache(w, r)
		}
	})

	http.HandleFunc("/invalidate", invalidateEntry)

	go http.ListenAndServe(":5000", nil)

	go purgeCache()

	log.Println("Caching service started, press <ENTER> to exit")
	go http.ListenAndServe(":3000", new(util.GzipHandler))

	waitCh := make(chan struct{})
	<-waitCh
}

func getFromCache(w http.ResponseWriter, r *http.Request) {
	mutex.RLock()
	defer mutex.RUnlock()

	rootSpan := util.GetSpanFromRPCReq(tracer, r, "get-from-cache")
	defer rootSpan.Finish()

	key := r.URL.Query().Get("key")

	fmt.Printf("Searching cache for %s...", key)

	rootSpan.LogEvent("Searching cache for " + key)
	if entry, ok := cache[key]; ok {
		fmt.Println("found")
		rootSpan.LogEvent("found")
		w.Write(entry.data)
		return
	}

	w.WriteHeader(http.StatusNotFound)
	fmt.Println("not found")
	rootSpan.LogEvent("not found")
}
func saveToCache(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	defer mutex.Unlock()

	rootSpan := util.GetSpanFromRPCReq(tracer, r, "save-to-cache")
	defer rootSpan.Finish()

	key := r.URL.Query().Get("key")
	cacheHeader := r.Header.Get("cache-control")

	rootSpan.LogEvent(
		fmt.Sprintf("Saving cache entry with key '%s' for %s seconds", key, cacheHeader),
	)

	matches := maxAgeRexexp.FindStringSubmatch(cacheHeader)
	if len(matches) == 2 {
		dur, _ := strconv.Atoi(matches[1])
		data, _ := ioutil.ReadAll(r.Body)
		cache[key] = &cacheEntry{data: data, expiration: time.Now().Add(time.Duration(dur) * time.Second)}
	}
}

func invalidateEntry(w http.ResponseWriter, r *http.Request) {
	mutex.Lock()
	defer mutex.Unlock()

	rootSpan := util.GetSpanFromRPCReq(tracer, r, "invalidate-cache-entry")
	defer rootSpan.Finish()

	key := r.URL.Query().Get("key")

	rootSpan.LogEvent(
		fmt.Sprintf("purging entry with key '%s'", key),
	)
	delete(cache, key)
}

func purgeCache() {
	for range tickCh {
		mutex.Lock()
		now := time.Now()

		fmt.Println("purging cache")

		for k, v := range cache {
			if now.Before(v.expiration) {
				fmt.Printf("purging entry with key '%s'\n", k)
				delete(cache, k)
			}
		}
		mutex.Unlock()
	}
}
